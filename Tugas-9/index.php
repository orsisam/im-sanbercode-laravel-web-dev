<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas ke 9</title>
</head>
<body>
    <h1>TUGAS Hari ke 9</h1>
   <h3>Release 0</h3>
   <?php
   require("animal.php");
   $sheep = new Animal("shaun");

   echo "Name: " . $sheep->get_name() . "<br>";
   echo "Legs: " . $sheep->get_legs() . "<br>";
   echo "Cold blooded: " . $sheep->get_cold_blooded() . "<br>";
   ?>

    <h3>Release 1</h3>

   <?php
   require("Ape.php");
   require("Frog.php");

   // Ape
   $kera = new Ape("kera sakti");
   $nameKera = $kera->get_name();
   $legsKera = $kera->get_legs();
   $coldBloodKera = $kera->get_cold_blooded();
   $yellKera = $kera->yell();

    // Frog
   $frog = new Frog("buduk");
   $nameFrog = $frog->get_name();
   $legsFrog = $frog->get_legs();
   $coldBloodFrog = $frog->get_cold_blooded();
   $jumpFrog = $frog->jump();

   ?>

    <strong>Class Ape</strong>
    <ul>
        <li>Name: <?php echo "$nameKera"; ?></li>
        <li>Legs: <?php echo "$legsKera"; ?></li>
        <li>Cold blooded: <?php echo "$coldBloodKera"; ?></li>
        <li>Yell: <?php echo "$yellKera"; ?></li>
    </ul>

    <strong>Class Frog </strong>
    <ul>
        <li>Name: <?php echo "$nameFrog"; ?></li>
        <li>Legs: <?php echo "$legsFrog"; ?></li>
        <li>Cold blooded: <?php echo "$coldBloodFrog"; ?></li>
        <li>Jump: <?php echo "$jumpFrog"; ?></li>
    </ul>
</body>
</html>