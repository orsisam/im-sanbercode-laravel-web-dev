<?php
require_once('animal.php');

class Frog extends Animal
{

    public function __construct($name)
    {
        parent::__construct($name);
        parent::setColdBlood("yes");
    }

    public function jump()
    {
        return "hop hop";
    }
}