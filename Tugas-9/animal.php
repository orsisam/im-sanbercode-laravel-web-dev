<?php

class Animal{
    private $legs = 4;
    private $cold_blooded = "no";
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function get_name(){
        return $this->name;
    }

    public function get_legs(){
        return $this->legs;
    }

    public function get_cold_blooded(){
        return $this->cold_blooded;
    }

    public function setLegs($legs){
        $this->legs = $legs;
    }

    public function setColdBlood($cold_blooded){
        $this->cold_blooded = $cold_blooded;
    }
}